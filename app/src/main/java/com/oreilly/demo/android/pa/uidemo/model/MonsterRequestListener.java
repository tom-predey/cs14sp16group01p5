package com.oreilly.demo.android.pa.uidemo.model;

/**
 * Created by jay on 5/2/2016.
 */
public interface MonsterRequestListener
{
    void onMonsterRequest(MonsterRequest r);
}
