package com.oreilly.demo.android.pa.uidemo.model;

import java.util.Random;

/**
 * Created by jay on 5/2/2016.
 */
public class Constants
{
    public static boolean isConstantsSet;
    public static final float PAD_SIZE = .25f;
    public static int H; //Height of the device
    public static int W; //Width of the device
    public static float HDPI;
    public static float WDPI;
    public static float HI;
    public static float WI;
    public static int ROWS;
    public static int COLS;
    public static int INIT_MONSTERS = 5;



    public static int N = 20; //Number of rows
    public static int M = 12; //Number of columns
    public static int K = 5; //Initial number of monsters in the game


    public static int cellWidth = W/M;
    public static int cellHeight = H/N;
    public static int gameCapasity = N*M;
    public static int D = cellWidth/2;
    public static final Random rand = new Random();

    //Takes a floating point x coordinate, turns it into a grid row
    public static int xToCol(final float x)
    {
        int col = -1;

        for(col=0; col < M; col++)
        {
            if(  x > col*cellWidth && x < (col+1)*cellWidth  )
            {
                return col;
            }
        }
        return col;
    }

    public static int yToRow(final float y)
    {
        int row = -1;

        for( row=0; row < N; row++)
        {
            if(  y > row*cellHeight && y < (row+1)*cellHeight  )
            {
                return row;
            }
        }
        return row;
    }



    public static float RowToY(final int row)
    {
        return row*cellHeight + (cellHeight/2);
    }

    public static float ColToX(final int col)
    {
        return col*cellWidth + (cellWidth/2);
    }








}
