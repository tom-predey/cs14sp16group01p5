package com.oreilly.demo.android.pa.uidemo.model;

public class Cell
{
    private Monster monster;

    private int row;

    private int col;

    public Cell()
    {
        row = -1;
        col = -1;
        monster = null;
    }

    public Cell(final int row, final int col)
    {
        synchronized (this)
        {
            if(row < 0)
            {
                throw new IndexOutOfBoundsException("rows cannot be less than 0!");
            }

            if(col < 0)
            {
                throw new IndexOutOfBoundsException("columns cannot be less than 0!");
            }

            this.row = row;
            this.col = col;
            this.monster = null;
        }
    }

    public Cell(final int row, final int col, Monster monster)
    {
        synchronized (this)
        {
            if (row < 0) {
                throw new IndexOutOfBoundsException("rows cannot be less than 0!");
            }

            if (col < 0) {
                throw new IndexOutOfBoundsException("columns cannot be less than 0!");
            }

            this.row = row;
            this.col = col;
            this.monster = monster;
        }
    }

    public synchronized Boolean isEmpty()
    {
        try
        {
            if (null == this.monster)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (NullPointerException e)
        {
            return true;
        }

    }

    public synchronized Cell getCell()  { return this; }

    public synchronized int getRow() { return this.row; }

    public synchronized int getCol() { return this.col; }

    public synchronized float getX() { return this.getCol()*Constants.cellWidth + (Constants.cellWidth/2); }

    public synchronized float getY() { return this.getCol()*Constants.cellHeight + (Constants.cellHeight/2);}

    //will always return the smaller dimention of the cell to garantee fit
    public static synchronized float getDiameter()
    {
        if(Constants.cellHeight < Constants.cellWidth){ return Constants.cellHeight/2; }

        if(Constants.cellHeight > Constants.cellWidth){ return Constants.cellWidth/2; }

        return Constants.cellHeight/2;
    }


    //removes the monster if it is Vulnerable
    public synchronized Boolean removeMonster()
    {
        try
        {
                if(null != monster && monster.isVulnerable())
                {
                    monster.cancel(true);
                    monster = null;
                    return true;
                }
                else
                {
                    return false;
                }
        }
        catch (NullPointerException e)
        {
            return false;
        }
    }



    //removes regardless of vunerability
    public synchronized Boolean killMonster()
    {
        try
        {
            if(null != monster)
            {
                monster.cancel(true);
                monster = null;
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (NullPointerException e)
        {
            return false;
        }
    }

    public synchronized Boolean isVulnerable()
    {
        try
        {
            if(null != monster)
            {

                return monster.isVulnerable();
            }
            else
            {
                return false;
            }
        }
        catch (NullPointerException e)
        {
            return false;
        }
    }


    public synchronized Boolean setMonster( Monster monster )
    {
        try
        {
                //point to the new monster
                this.monster = monster;
                //set the monster's internal cell to the current cell
                this.monster.setCell(this);

            //if the monster is not running, then start it
            if( !this.monster.isRunning  )
            {
               this.monster.execute();
            }
            return true;
        }
        catch (NullPointerException e)
        {
            return false;
        }
    }

    //clears the monster out of the cell if the monster in the cell
    //does not match the cell the monster think's it has
    public synchronized Boolean validateMonster()
    {
        if( this.monster.getCell() != this )
        {
            this.monster = null;
            return false;
        }
        else
        {
            return true;
        }
    }

    //returns the monster in the cell
    public synchronized Monster getMonster()
    {
        return this.monster;
    }

}
