package com.oreilly.demo.android.pa.uidemo.model;

/**
 * Created by jay on 5/4/2016.
 */

import java.util.Random;
import java.util.*;

public class MonsterCells
{

    /** MonstersChangeListener. */
    public interface MonstersChangeListener
    {
        /** @param monsters that changed. */
        void onMonstersChange(MonsterCells monsters);
    }

    private Cell[][] monsterCells;

    public MonsterCells()
    {
        monsterCells = new Cell[Constants.N][Constants.M];

            for (int row = 0; row < Constants.N; row++)
            {
                for (int col = 0; col < Constants.M; col++)
                {
                    monsterCells[row][col] = new Cell(row,col);
                }
            }
    }



    private MonstersChangeListener monstersChangeListener;
    private MonsterRequestChannel monsterRequestChannel;

    //this class will allow us to control how the individual requests are handled.
    public class  MonsterRequestChannel implements MonsterRequestListener
    {
        @Override
        public synchronized void onMonsterRequest(MonsterRequest r)
        {
            //use the try blocks to help prevent out of bounds exceptions
            try
            {
                //check if the requested cell is empty
                    if ( r != null)
                    {
                        moveMonster(r.getStartCell(), r.getEndCell(monsterCells));
                    }
            }
            //the request that was made was out of bounds, therefore we do not take any action
            catch (IndexOutOfBoundsException e)
            {

            }
        }
    }


    //used to move a monster from one cell to another
    public synchronized Boolean moveMonster(Cell startCell, Cell endCell)
    {
        try
        {
            //if the desination cell does not have any monsters
            //this also prevents a monster from trying to move to the same position because
            //itself would already be in it!
            if(endCell.isEmpty())
            {
                //set the destination cell's monster to the monster that made the request
                //the monster is technically in two cells now
                if( endCell.setMonster( startCell.getMonster() ));
                {
                    //if transferring the monster to a new cell worked
                    //then we need to release the monster from the starting cell
                    //so that it can live happily in its new cell.
                    //we do this by asking the starting cell to validate itself which should fail
                    startCell.validateMonster();
                    notifyListener();
                }
            }
            //the destination cell has a monster in it
            else
            {
                return false;
            }
        }
        catch (IndexOutOfBoundsException e)
        {

        }
        return true;
    }


    /** @param m set the change listener. */
    public void setMonstersChangeListener(final MonstersChangeListener m)
    {
        monstersChangeListener = m;
    }



    /** @param m set the change listener. */
    public void setMonsterRequestListener(final MonsterRequestChannel r)
    {
        monsterRequestChannel = r;
    }




    /** remove the monster at a location if it is there and vulnerable */
    public synchronized Boolean removeMonster(final int row, final int col)
    {
        try
        {
            Cell LocalCell = monsterCells[row][col];

            if (!LocalCell.isEmpty() && LocalCell.isVulnerable())
            {
                LocalCell.removeMonster();
                notifyListener();
                return true;
            }
            return false;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return false;
        }
    }

    public synchronized Boolean removeMonster(final int row, final int col, final Boolean isForceRemove)
    {
        try
        {
            Cell LocalCell = monsterCells[row][col];

                if (!LocalCell.isEmpty() && LocalCell.isVulnerable())
                {
                    LocalCell.removeMonster();
                    notifyListener();
                    return true;
                }
                return false;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return false;
        }
    }

    /** check if a grid location has a monster in it */
    public synchronized boolean isEmpty(final int row, final int col)
    {
        try
        {
            Cell LocalCell = monsterCells[row][col].getCell();


                if (LocalCell.isEmpty())
                {
                    return true;
                }
                else
                {
                    return false;
                }

        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return true;
        }
        catch (NullPointerException e)
        {
            return true;
        }
    }

    /** @return grid representation of monsters. */
    public synchronized Cell[][] getMonsters() { return monsterCells; }




    public synchronized Boolean addMonster(final int row, final int col)
    {
        try
        {
            Cell LocalCell = monsterCells[row][col].getCell();

                if (LocalCell.isEmpty())
                {
                    LocalCell.setMonster(new Monster(row,col));
                }
                notifyListener();

            return true;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return false;
        }
    }






    /** Remove all monsters. */
    public synchronized Boolean clearMonsters()
    {
        try
        {
            int removals = 0;
            for (int row = 0; row < Constants.N; row++)
            {
                for (int col = 0; col < Constants.M; col++)
                {
                    if(this.removeMonster(row,col,true))
                    {
                        notifyListener();
                        removals++;
                    }
                }
            }

            if(0 != removals)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            return false;
        }

    }


    public synchronized int size()
    {
        try
        {
            int num = 0;
            for (int row = 0; row < Constants.N; row++)
            {
                for (int col = 0; col < Constants.M; col++)
                {
                    if(!this.isEmpty(row,col))
                    {
                        num++;
                    }
                }
            }
            return num;
        }
        catch (IndexOutOfBoundsException e)
        {
            return -1;
        }
    }


    private void notifyListener()
    {
        if (null != monstersChangeListener)
        {
            monstersChangeListener.onMonstersChange(this);
        }
    }

    public Boolean setUpMonsters()
    {
        try
        {
            int i=0;
            while(i < Constants.K && this.size() < Constants.gameCapasity)
            {
                if( addMonster( Constants.rand.nextInt(Constants.N),Constants.rand.nextInt(Constants.M) ) )
                {
                    i++;
                }
            }
            return true;
        }
        catch (IndexOutOfBoundsException e)
        {
         return false;
        }
    }


    public synchronized Cell getCell(final int row, final int col)
    {
        try
        {
           return monsterCells[row][col];
        }
        catch (IndexOutOfBoundsException e)
        {
            return null ;
        }
    }
}
