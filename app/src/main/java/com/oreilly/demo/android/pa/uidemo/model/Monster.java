package com.oreilly.demo.android.pa.uidemo.model;
import android.os.AsyncTask;

import java.util.Random;

//TODO merge other Monster class with this one
    /** A dot: the coordinates, color and size. */
    public final class Monster extends AsyncTask<Boolean, Boolean, Boolean> implements Actor
    {

        @Override
        protected void onPreExecute() { isRunning = false;}

        //TODO add the ability for the monster to request to move to a new square
        //the meat of the monster
        @Override
        protected Boolean doInBackground(Boolean...Params)
        {
            try
            {
                //while the task is not canceled e.g. running and the monster is not vulnerable
                while( !isCancelled() )
                {
                    isRunning = true;
                    //maximum allowed sleeping time
                    int maxSleepTime = 3000;

                    //minimum allowed sleeping time
                    int minSleepTime = 0;

                    //the time to sleep
                    int sleepTime = minSleepTime + (int)(Math.random() * maxSleepTime);

                    //set the state for the next time period
                    Random rand = new Random();
                    //vulnerable = rand.nextBoolean();
                    this.FlipState();

                    //make a new cell
                    Cell endCell = new Cell(row + rand.nextInt(1) - rand.nextInt(1), col + rand.nextInt(1) - rand.nextInt(1));
                    MonsterRequest Move = new MonsterRequest(cell, endCell);

                    notifyMonsterRequestListener(Move);

                    //sleep for a given amount of time
                    Thread.sleep(sleepTime);
                    //returns true of the time expired. This means that the monster reached the next state
                }
                return true;
            }
            //if the process gets interrupted, then return false
            catch (InterruptedException e)
            {
                isRunning = false;
                return false;
            }
        }

        //if the task executes successfully, then the result of the execution comes here
        @Override
        protected void onPostExecute(Boolean result) { isRunning = false; }

        //if the click event cancels the thread, onCancelled() is called.
        //this is where the monster would destroy itself if it is vulnerable
        @Override
        protected void onCancelled() { isRunning = true; }

        public Boolean FlipState()
        {
            if(vulnerable)
            {
                vulnerable = false;
            }
            else
            {
                vulnerable = true;
            }
            //returns the new state
            return vulnerable;
        }

        //easy way to acess the state of the thread
        public static Boolean isRunning;

        private int row, col;
        private boolean vulnerable;
        private MonsterRequestListener monsterRequestListener;

        private Cell cell;

        //get the monster's cell
        public synchronized Cell getCell()
        {
            return cell;
        }

        //set the monster's cell
        public synchronized void setCell( Cell cell )
        {
            this.cell = cell;
        }




        public Monster(Cell cell)
        {
            this.cell = cell;
            this.vulnerable = false;
        }


        public Monster(final int row, final int col)
        {


            this.row = row;
            this.col = col;
            this.vulnerable = false;
        }

        public Monster(final int row, final int col, final boolean vulnerable)
        {
            this.row = row;
            this.col = col;
            this.vulnerable = vulnerable;
        }

        public Monster(final int row, final int col, final boolean vulnerable, final MonsterRequestListener r)
        {
            this.row = row;
            this.col = col;
            this.vulnerable = vulnerable;

            setMonsterRequestListener(r);
        }

        /** @return the horizontal coordinate. */
        public int getRow() { return cell.getRow(); }

        /** @return the vertical coordinate. */
        public int getCol() { return cell.getCol(); }

        public float getX() { return this.getCol()*Constants.cellWidth + (Constants.cellWidth/2); }

        public float getY() { return this.getCol()*Constants.cellHeight + (Constants.cellHeight/2);}

        public float getDiameter()
        {
            if(Constants.cellHeight > Constants.cellWidth)
            {
                return Constants.cellWidth/2;
            }
            else if(Constants.cellHeight < Constants.cellWidth)
            {
                return Constants.cellHeight/2;
            }
            else
            {
                return Constants.cellWidth/2;
            }
        }

        /** @return the vulnerability status. */
        public synchronized boolean isVulnerable() { return vulnerable; }

        public void setMonsterRequestListener(final MonsterRequestListener r)
        {
                monsterRequestListener = r;
        }

        public void notifyMonsterRequestListener( MonsterRequest r )
        {
            if (null != monsterRequestListener)
            {
               monsterRequestListener.onMonsterRequest(r);
            }
        }


        @Override
        public <ActorVisitorObject> ActorVisitorObject accept(final ActorVisitor<ActorVisitorObject> v) { return v.onMonster(this); }

    }

