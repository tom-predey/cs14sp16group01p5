package com.oreilly.demo.android.pa.uidemo.view;
import com.oreilly.demo.android.pa.uidemo.model.*;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;



/**
 * I see spots!
 *
 * @author <a href="mailto:android@callmeike.net">Blake Meike</a>
 */
public class MonsterView extends View
{
    private volatile MonsterCells monsters;


    public MonsterView(final Context context)
    {
        super(context);
        setFocusableInTouchMode(true);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
    }


    public MonsterView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
        setFocusableInTouchMode(true);
    }


    public MonsterView(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
        setFocusableInTouchMode(true);
    }


    public void setConstants()
    {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Constants.W = this.getWidth();
        Constants.H = this.getHeight();
        //dots per inch in x and y
        Constants.WDPI = metrics.xdpi;
        Constants.HDPI = metrics.ydpi;
        //dimentions in inches
        Constants.WI = Constants.W/Constants.WDPI;
        Constants.HI = Constants.H/Constants.HDPI;
        //number of rows and columns
        Constants.COLS = (int)Math.floor(Constants.WI/Constants.PAD_SIZE);
        Constants.ROWS = (int)Math.floor(Constants.HI/Constants.PAD_SIZE);
        //flag that the variables have been set
        Constants.isConstantsSet = true;
    }

    public void setMonsters(final MonsterCells monsters) { this.monsters = monsters; }

    /**
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override protected void onDraw(final Canvas canvas)
    {
       // this.setConstants();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Constants.W = this.getWidth();
        Constants.H = this.getHeight();
        //dots per inch in x and y
        Constants.WDPI = metrics.xdpi;
        Constants.HDPI = metrics.ydpi;
        //dimentions in inches
        Constants.WI = Constants.W/Constants.WDPI;
        Constants.HI = Constants.H/Constants.HDPI;
        //number of rows and columns
        Constants.COLS = (int)Math.floor(Constants.WI/Constants.PAD_SIZE);
        Constants.ROWS = (int)Math.floor(Constants.HI/Constants.PAD_SIZE);
        //flag that the variables have been set
        Constants.isConstantsSet = true;

        try
        {

            Paint paintUtility = new Paint();
            Paint monsterPaint = new Paint();

            paintUtility.setStyle(Style.STROKE);
            paintUtility.setColor(hasFocus() ? Color.BLUE : Color.GRAY);
            canvas.drawRect(0, 0, getWidth() - 1, getHeight() -1, paintUtility);

            monsterPaint.setStyle(Style.FILL);




            for (int row = 0; row < Constants.ROWS; row++)
            {
                for (int col = 0; col < Constants.M; col++)
                {
                    //if there is no monster then just draw the background
                    if( monsters.getCell(row,col).isEmpty() )
                    {
                        float y = Constants.RowToY(row);
                        float x = Constants.ColToX(col);
                        monsterPaint.setColor(Color.GREEN);
                        //Monster TempMonster = new Monster(row,col);
                        canvas.drawCircle( x, y, Cell.getDiameter() , monsterPaint);
                    }
                    //if there is a monster, draw it
                    else
                    {
                        //vulnerable
                        if(monsters.getCell(row,col).isVulnerable())
                        {
                            monsterPaint.setColor(Color.BLUE);
                        }
                        //not vulnerable
                        else
                        {
                            monsterPaint.setColor(Color.RED);
                        }
                        canvas.drawCircle( monsters.getCell(row,col).getX(),monsters.getCell(row,col).getY(), Cell.getDiameter(), monsterPaint);
                    }
                }
            }
            //save the last gamestate
            //LastMonsterBoard = MonsterBoard.clone();
        }
        catch (NullPointerException e)
        {

        }

    }
}
