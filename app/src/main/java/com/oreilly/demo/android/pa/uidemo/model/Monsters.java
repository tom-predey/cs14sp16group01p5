//package com.oreilly.demo.android.pa.uidemo.model;
//
//import java.util.Random;
//import java.util.*;
//
//
///**
// * Created by jay on 5/2/2016.
// */
//public class Monsters
//{
//    /** MonstersChangeListener. */
//    public interface MonstersChangeListener
//    {
//        /** @param monsters that changed. */
//        void onMonstersChange(Monsters monsters);
//    }
//
//    private final Monster[][] monsters = new Monster[Constants.N][Constants.M];
//
//
//
//    private MonstersChangeListener monstersChangeListener;
//    private MonsterRequestChannel monsterRequestChannel;
//
//    //this class will allow us to control how the individual requests are handled.
//    public class MonsterRequestChannel implements MonsterRequestListener
//    {
//        @Override
//        public void onMonsterRequest(MonsterRequest r)
//        {
//            //use the try blocks to help prevent out of bounds exceptions
//            try
//            {
//                //check if the requested cell is empty
//                if (monsters[r.getRowMove()][r.getColMove()] == null)
//                {
//                    //get the monster that make the request
//                    Monster RequestingMonster = r.getRequestMonster();
//                    //set the requesting monster's row to the requested row.
//                    RequestingMonster.setRow( r.getRowMove() );
//                    //set the requesting monster's col to the requested col.
//                    RequestingMonster.setCol( r.getColMove() );
//                    //set the new cell to the monster that made the request
//                    monsters[r.getRowMove()][r.getColMove()] = RequestingMonster;
//                    //clear out the old cell
//                    monsters[r.getRequestMonster().getRow()][r.getRequestMonster().getCol()] = null;
//                    notifyListener();
//                }
//            }
//            //the request that was made was out of bounds, therefore we do not take any action
//            catch (IndexOutOfBoundsException e)
//            {
//
//            }
//        }
//    }
//
//
//
//
//
//
//    /** @param m set the change listener. */
//    public void setMonstersChangeListener(final MonstersChangeListener m)
//    {
//        monstersChangeListener = m;
//    }
//
//
//
//    /** @param m set the change listener. */
//    public void setMonsterRequestListener(final MonsterRequestChannel r)
//    {
//        monsterRequestChannel = r;
//    }
//
//
//
//
//    /** remove the monster at a location if it is there and vulnerable */
//    public Boolean removeMonster(final int row, final int col)
//    {
//        try
//        {
//            if (monsters[row][col] != null && monsters[row][col].isVulnerable())
//            {
//                monsters[row][col].cancel(true);
//                monsters[row][col] = null;
//                notifyListener();
//                return true;
//            }
//            return false;
//        }
//        catch (ArrayIndexOutOfBoundsException e)
//        {
//            return false;
//        }
//    }
//
//    public Boolean removeMonster(final int row, final int col, Boolean isForceRemove)
//    {
//        try
//        {
//            if (!isForceRemove && monsters[row][col] != null && monsters[row][col].isVulnerable())
//            {
//                monsters[row][col].cancel(true);
//                monsters[row][col] = null;
//                notifyListener();
//                return true;
//            }
//            else if (isForceRemove && monsters[row][col] != null )
//            {
//                monsters[row][col].cancel(true);
//                monsters[row][col] = null;
//                notifyListener();
//                return true;
//            }
//            else
//            {
//                monsters[row][col] = null;
//                return false;
//            }
//        }
//        catch (ArrayIndexOutOfBoundsException e)
//        {
//            return false;
//        }
//    }
//
//    /** check if a grid location has a monster in it */
//    public boolean isEmpty(int row, int col)
//    {
//        try
//        {
//            if (monsters[row][col] == null)
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }
//        catch (ArrayIndexOutOfBoundsException e)
//        {
//            return true;
//        }
//    }
//    //TODO maybe this needs to go in the MainActivity class?
//    /** Create an initial game state for the monsters game */
//    public void setUpMonsters()
//    {
//        Random rand = new Random();
//        int collisions = 0; //In case a space is occupied
//        //Place K monsters in the grid
//        for (int k = 0; k < Constants.K + collisions; k++)
//        {
//            int row = Math.abs(rand.nextInt() % Constants.N);
//            int col = Math.abs(rand.nextInt() % Constants.M);
//            if (!isEmpty(row, col))
//            {
//                collisions++; //Occupied cell encountered
//            }
//            else
//            {
//                monsters[row][col] = new Monster(row, col); //Place new monster
//                notifyListener();
//            }
//        }
//    }
//
//
//    /** @return grid representation of monsters. */
//    public Monster[][] getMonsters() { return monsters; }
//
//
//    public List<Monster> getMonstersAsList()
//    {
//        return twoDArrayToList(monsters);
//    }
//
//
//    public <T> List<T> twoDArrayToList(T[][] twoDArray)
//    {
//        List<T> list = new ArrayList<T>();
//        for (T[] array : twoDArray)
//        {
//            list.addAll(Arrays.asList(array));
//        }
//        return list;
//    }
//
//
//    public Boolean addMonster(final int row, final int col)
//    {
//        try
//        {
//            monsters[row][col] = new Monster(row,col);
//            //start the monster. It Lives!!
//            monsters[row][col].execute();
//            notifyListener();
//            return true;
//        }
//        catch (ArrayIndexOutOfBoundsException e)
//        {
//            return false;
//        }
//    }
//
//
//
//
//
//
//    /** Remove all monsters. */
//    public Boolean clearMonsters()
//    {
//        try
//        {
//            for (int row = 0; row < Constants.N; row++)
//            {
//                for (int col = 0; col < Constants.M; col++)
//                {
//                    this.removeMonster(row,col,true);
//                }
//            }
//            notifyListener();
//            return true;
//        }
//        catch (IndexOutOfBoundsException e)
//        {
//            return false;
//        }
//
//    }
//
//
//    public int size()
//    {
//        try
//        {
//            int num = 0;
//            for (int row = 0; row < Constants.N; row++)
//            {
//                for (int col = 0; col < Constants.M; col++)
//                {
//                    if(!this.isEmpty(row,col))
//                    {
//                        num++;
//                    }
//                }
//            }
//            return num;
//        }
//        catch (IndexOutOfBoundsException e)
//        {
//            return -1;
//        }
//    }
//
//
//    private void notifyListener()
//    {
//        if (null != monstersChangeListener)
//        {
//            monstersChangeListener.onMonstersChange(this);
//        }
//    }
//}
