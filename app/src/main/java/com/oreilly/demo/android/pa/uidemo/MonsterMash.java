package com.oreilly.demo.android.pa.uidemo;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.EditText;

import com.oreilly.demo.android.pa.uidemo.model.Constants;
import com.oreilly.demo.android.pa.uidemo.model.Monster;
import com.oreilly.demo.android.pa.uidemo.model.MonsterCells;
import com.oreilly.demo.android.pa.uidemo.view.MonsterView;


/** Android UI demo program */
public class MonsterMash extends Activity
{
    /** Listen for taps. */
    private static final class TrackingTouchListener implements View.OnTouchListener {
        private final MonsterCells mMonsters;
        private List<Integer> tracks = new ArrayList<>();

        TrackingTouchListener(final MonsterCells monsters) { mMonsters = monsters; }

        @Override public boolean onTouch(final View v, final MotionEvent evt)
        {
            mMonsters.removeMonster(Constants.yToRow(evt.getY()), Constants.xToCol(evt.getX()));
            return true;
        }

        private void addMonster(final MonsterCells monsters, final float x, final float y, final float p, final float s)
        {
            final int row = Constants.yToRow(y);
            final int col = Constants.xToCol(x);
            monsters.addMonster(row,col);
        }

        private Boolean removeMonster(final MonsterCells monsters,final float x,final float y)
        {
            final int row = Constants.yToRow(y);
            final int col = Constants.xToCol(x);

            return monsters.removeMonster(row,col);
        }

    }

    private final Random rand = new Random();

    /** The application model */
    //private MonsterCells monsterModel = new MonsterCells();

    /** The application view */
    private MonsterView monsterView;


    private MonsterCells monsterModel;

    /** Called when the activity is first created. */
    @Override public void onCreate(final Bundle state)
    {
        super.onCreate(state);

        Constants.isConstantsSet = false;

        monsterModel = new MonsterCells();

        //install the view
        setContentView(R.layout.main);

        //find the monsters view
        monsterView = (MonsterView) findViewById(R.id.monsters);

        //monsterView.setConstants();

        monsterModel = new MonsterCells();
        monsterView.setMonsters(monsterModel);

        monsterModel = new MonsterCells();
        monsterView.setMonsters(monsterModel);

        monsterView.setOnCreateContextMenuListener(this);
        monsterView.setOnTouchListener(new TrackingTouchListener(monsterModel));

        monsterView.setOnKeyListener((final View v, final int keyCode, final KeyEvent event) -> {
            if (KeyEvent.ACTION_DOWN != event.getAction()) {
                return false;
            }

            int color;
            switch (keyCode) {
                case KeyEvent.KEYCODE_SPACE:
                    color = Color.MAGENTA;
                    break;
                case KeyEvent.KEYCODE_ENTER:
                    color = Color.BLUE;
                    break;
                default:
                    return false;
            }

            makeMonster(monsterModel, monsterView);

            return true;
        });

        // wire up the controller
        //initialize the monsters when we click start
        findViewById(R.id.menu_start).setOnClickListener((final View v) ->
                monsterModel.setUpMonsters()
        );
        //clear all the monsters when we click the clear button
        findViewById(R.id.menu_clear).setOnClickListener((final View v) ->
                clearMonsters(monsterModel, monsterView)
        );

        final EditText tb1 = (EditText) findViewById(R.id.text1);
        final EditText tb2 = (EditText) findViewById(R.id.text2);
        monsterModel.setMonstersChangeListener((final MonsterCells monsters) ->
        {
            tb1.setText( "Monsters:" + Integer.toString(monsterModel.size())   );
            tb2.setText("0");
        });
    }

    @Override public void onResume()  { super.onResume(); }

    @Override public void onPause()
    {
        super.onPause();
    }

    /** Install an options menu. */
    @Override public boolean onCreateOptionsMenu(final Menu menu)
    {
        getMenuInflater().inflate(R.menu.simple_menu, menu);
        return true;
    }

    /** Respond to an options menu selection. */
    @Override public boolean onOptionsItemSelected(final MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_clear:
                monsterModel.clearMonsters();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Install a context menu. */
    @Override public void onCreateContextMenu( final ContextMenu menu, final View v, final ContextMenuInfo menuInfo)
    {
        menu.add(Menu.NONE, 1, Menu.NONE, "Clear").setAlphabeticShortcut('x');
    }

    /** Respond to a context menu selection. */
    @Override public boolean onContextItemSelected(final MenuItem item)
    {
        switch (item.getItemId())
        {
            case 1:
                monsterModel.clearMonsters();
                return true;
            default:
                return false;
        }
    }

    /**
     * @param monsters the monsters we're drawing
     * @param view the view in which we're drawing monsters
     * @param color the color of the dot
     */
    //makes a monster
    public Boolean makeMonster(MonsterCells monsters, final MonsterView view)
    {
        int row = rand.nextInt(Constants.ROWS);
        int col = rand.nextInt(Constants.COLS);

        while (monsters.isEmpty(row,col))
        {
            return monsters.addMonster(row,col);
        }
        return false;
    }

    //makes a specified number of monsters
    public Boolean makeMonster(final MonsterCells monsters, final MonsterView view, final int number)
    {
        //make the specified number of monsters
        int i = 0;
        while( i < number && i < Constants.gameCapasity)
        {
            //only count up if making the monster returns true
            if( this.makeMonster(monsters, view)  )
            {
                i++;
            }
        }
        return true;
    }

    //makes a specified number of monsters
    public Boolean resetMonster(final MonsterCells monsters, final MonsterView view, final int number)
    {
        this.clearMonsters(monsters,view);

        //make the specified number of monsters
        int i = 0;
        while( i < number && i < Constants.gameCapasity)
        {
            //only count up if making the monster returns true
            if( this.makeMonster(monsters, view)  )
            {
                i++;
            }
        }
        return true;
    }

    public Boolean clearMonsters(final MonsterCells monsters, final MonsterView view)
    {
        //clear the monsters
        monsters.clearMonsters();

        //update the monsters in the view
        //view.setMonsters(monsters);
        return true;
    }



}
