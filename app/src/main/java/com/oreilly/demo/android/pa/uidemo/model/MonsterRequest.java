package com.oreilly.demo.android.pa.uidemo.model;

/**
 * Created by jay on 5/2/2016.
 */
public class MonsterRequest
{
    Cell startCell;
    Cell endCell;

    public MonsterRequest(Cell startCell, Cell endCell)
    {
        this.startCell = startCell;
        this.endCell = endCell;
    }

    public synchronized Cell getStartCell(){return this.startCell;}
    //the endCell on its own cannot represent the ending since the monster has no connection
    //to the actual game board so it must call a function with the field in it to get the actual
    //cell that is in the game. which is getEndCell;
    public synchronized Cell getEndCell(final Cell[][] Field)
    {
        try
        {
            return Field[endCell.getRow()][endCell.getCol()];
        }
        catch (IndexOutOfBoundsException e)
        {
            return null;
        }
    }
}
