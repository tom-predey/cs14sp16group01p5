# Project 5
## Due Dates (FIRM)

Tuesday, May 3: final Project 5 submission by 4pm
Tuesday, May 3, 4:15pm (day of the final exam): brief in-class presentation and demo (required for full credit)
Team presentations must also be Submitted on Sakai by 4pm on May 3
Missing the final submission deadline is likely to result in an incomplete grade for the semester.

## Larger Team Project (same teams as for in-class state machine and unit testing exercises)

## Objectives

### Familiarity with
* object-oriented modeling and design
* SOLID and other design principles
* event-driven program execution
* thread-based concurrency
* Model-View-Controller architecture
* Android graphics and touch events
## Description

In this project, you will implement a simple interactive game as an Android application. The idea is to use your imagination and have lots of fun!

Use this Android Studio Project as the starting point or skeleton code for your game: https://github.com/LoyolaChicagoCode/uidemo-android-java.
## Functional Requirements (40%)

The object of the game is to eliminate as many monsters as possible on a two-dimensional playing field within limited time. Scoring is based on eliminating more monsters in less time.
* (1) Playing field: 

* (0.5) The screen is divided into n by m squares. 

* (0.5) n and m are chosen based on the device's display size such that the resulting squares can be pressed accurately with the user's finger. 

* (0.5) Each square has room for up to one monster. (optional extra credit)

* (2) Monsters: 

* (0.5) There are k monsters initially. Monsters behave autonomously in the following way: 

* (1.0) Monsters move around among adjacent squares at random (all eight neighbors are considered adjacent). 

* (0.5) Over time, Monsters repeatedly alternate between a vulnerable and protected state (shown in yellow versus green); they spend a certain percentage of time in the vulnerable state but alternate in a randomized way.

* (0.5) Touch interface: Monsters can be eliminated while in the vulnerable state by pressing on (or clicking) the square they occupy.

* (0.5) The playing field is locked into the preferred orientation to preserve the application state.

* Levels (extra credit): At higher levels, the number of Monsters goes up, and the proportion of time in the vulnerable state goes down.


* Display pixel density (extra credit): n and m are chosen by taking the device's pixel density into account such that the resulting squares are about the same physical size on different devices.

* Rotation (extra credit): the application state is preserved during device rotation.

* Further details will be fleshed out per discussion below.
## Nonfunctional Requirements (40%)

* (0.5) Follow the design principles discussed so far.

* (1.0) Maintain a clear, responsibility-based separation among the different building blocks.

* Follow the Model-View-Controller architecture (http://en.wikipedia.org/wiki/Model-view-controller) discussed in Mednieks's text (especially chapters 6 and 8). The example from the text is here: https://github.com/bmeike/ProgrammingAndroid2Examples/tree/master/AndroidUIDemo

* You may be able to reuse some of the model classes from this example: https://github.com/LoyolaChicagoCode/ecosystem-java

* (1.5) Ensure your application includes comprehensive unit, integration, and functional tests using the techniques from the clickcounter and stopwatch examples where appropriate. (See also http://developer.android.com/tools/testing/testing_android.html) At least one of Robolectric tests or Android instrumentation tests (in src/androidTest) should be present.

* Use thread-based concurrency for making the Monsters autonomous, and use suitable concurrency building blocks for ensuring the capacity of squares in a thread-safe way.

* (0.5) It is usually best to represent each Monster as a background task: http://developer.android.com/reference/android/os/AsyncTask.html

* (0.5) A good way to limit cell capacity is by giving each cell a semaphore to control access: http://developer.android.com/reference/java/util/concurrent/Semaphore.html

* Generally follow the coding guidelines and good Android development practice: http://developer.android.com

* Further details will be fleshed out per discussion below. 
## Written Part/Documentation (20%)

Please include these deliverables in the doc folder of your project5.

* (0.5) Provide a brief description of your domain model.

* (0.25) Use inline comments to document design details in the code.

* (0.25) Use javadoc comments to document how to use the abstractions (interfaces, classes) you designed.

* (1.0) Include a brief (300-500 words) report on the design tradeoffs and decisions you made in this project, including
MVA versus MVC
use of SOLID and other design principles
impact on testability
concurrency
Grading Criteria

Stated percentages for the major categories
Stated points for the specific items
Total 10 points
Deductions of up to 1 point for
major deviation from the required project folder structure (see stopwatch and clickcounter for examples)
inability to run and/or test
## How to submit

As the first step in working on this project, one of your larger team members will push the code skeleton to a private Bitbucket repository shared among all of you and your instructor and TA. The name of the repository is cs413sp16groupNp5, where N is your group number found in the slides from Weeks 11 and 12. When your work is ready to be graded, please notify your instructor and TA via Piazza.